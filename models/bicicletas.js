const e = require("express");

var Bicicletas = function(id, color, modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion =ubicacion;

}
Bicicletas.prototype.toString = function(){
    return 'id: ' + this.id + " | Color: " + this.color;
}

Bicicletas.allBicis =[]; 
Bicicletas.add = function(aBici){
    Bicicletas.allBicis.push(aBici);    
}

Bicicletas.finById = function(aBiciId){
    var aBici = Bicicletas.allBicis.find(x => x.id == aBiciId);
    if(aBici)
       return aBici;
    else
       throw new  Error(`No existe una bicicleta con el id ${aBiciId}`);
}

Bicicletas.removeById = function(aBiciId){
    for( var i = 0; i < Bicicletas.allBicis.length; i++){
       if (Bicicletas.allBicis[i].id == aBiciId) {
           Bicicletas.allBicis.splice(i,1);
           break;
       }
    }
}
 var  a = new Bicicletas(1, 'rojo' , 'urbana', [6.268527752547997, -75.56628236136316]);
 var  b= new Bicicletas(2, 'blanca' , 'urbana', [6.266309492515434, -75.56302079546276])

 Bicicletas.add(a);
 Bicicletas.add(b);


module.exports = Bicicletas;
