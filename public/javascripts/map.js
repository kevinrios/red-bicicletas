var map = L.map('main_map').setView([6.27654753701175, -75.55375108185116], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
}).addTo(map);

/*L.marker([6.258460189008073, -75.55066117731394]).addTo(map);
L.marker([6.260166569372608, -75.59323319538217]).addTo(map);
L.marker([6.2407135031132706, -75.5722905090744]).addTo(map);*/


$.ajax({
    dataType: "json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function (bici) {
          L.marker(bici.ubicacion ,{title: bici.id}).addTo(map);
        });
            
        
    }
})
